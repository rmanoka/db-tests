table! {
    data (id) {
        id -> Int4,
        title -> Varchar,
        datum -> Nullable<Int4>,
    }
}
