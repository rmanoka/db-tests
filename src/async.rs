extern crate futures;
extern crate futures_state_stream;
extern crate tokio_core;
extern crate tokio_postgres;

use self::futures::Future;
use self::tokio_core::reactor::Handle;
use self::tokio_postgres::{Connection, TlsMode, Error};
use std::env;

pub fn establish_connection(h: &Handle) ->
    Box<Future<Item = Connection, Error = Error> + Send>
{
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    Connection::connect(
        database_url,
        TlsMode::None,
        h
    )
}