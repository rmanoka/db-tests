extern crate diesel;
extern crate db_tests;

use self::db_tests::*;
use self::models::*;
use self::diesel::prelude::*;

fn main() {
    use db_tests::schema::data::dsl::*;

    let connection = db::establish_connection();
    let results = data
        .load::<Datum>(&connection)
        .expect("Error loading data");

    // println!("Displaying {} data", results.len());
    let mut sum: usize = 0;
    for post in results {
        match post.datum {
          Some(d) => if d == 0 { sum += d as usize; }
          None => (),
        }
    }
    println!("Zeroes {}", sum);
}