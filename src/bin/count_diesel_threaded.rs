extern crate diesel;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate db_tests;

use self::db_tests::*;
use self::diesel::prelude::*;

use std::thread;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use std::time::{Instant};

const TOTAL : i64 = 2097152;
const THREADS : i64 = 2;
const LIMIT : i64 = TOTAL / THREADS;

use diesel::deserialize::{FromSqlRow,FromSql};
use diesel::deserialize;
use diesel::sql_types;
use diesel::pg::Pg;
use diesel::row::Row;
use diesel::query_builder::AsQuery;

// impl FromSqlRow for Fn()
use db_tests::schema::data::table as data;
enum Sum {
    Sum(usize)
}

impl FromSqlRow<<data as AsQuery>::SqlType, Pg> for Sum {
    const FIELDS_NEEDED: usize = 3;

    fn build_from_row<R: Row<Pg>>(row: &mut R) -> deserialize::Result<Self> {
        let id: i32 = FromSql::<sql_types::Int4, Pg>::from_sql(row.take())?;
        let sum = if id % 2 == 0 {
            row.advance(1);
            let datum: Option<i32> = FromSql::<sql_types::Nullable<sql_types::Int4>, Pg>::from_sql(row.take())?;
            match datum {
                Some(d) => d as usize,
                _ => 0,
            }
        } else {
            let title: String = FromSql::<sql_types::VarChar, Pg>::from_sql(row.take())?;
            row.advance(1);
            title.len()
        };
        Ok(Sum::Sum(sum))
    }
}

impl Queryable<<data as AsQuery>::SqlType, Pg> for Sum {
    type Row = Sum;
    fn build(row: Self::Row) -> Self {
        row
    }
}

fn do_sum(i: i64) -> usize {
    use db_tests::schema::data::dsl::*;
    let connection = db::establish_connection();
    let results = data
        .offset(i*LIMIT)
        .limit(LIMIT)
        .load::<Sum>(&connection)
        .expect("Error loading data");
    let mut sum: usize = 0;

    for d in results {
        match d {
            Sum::Sum(num) => {sum += num}
        }
    }
    sum
}

fn main() {
    let mut children = vec![];


    let total_sum = Arc::new(AtomicUsize::new(0));
    let timer = Instant::now();

    for i in 0..THREADS {
        let total_sum = total_sum.clone();
        children.push(
            thread::spawn(move || {
                let sum = do_sum(i);
                total_sum.fetch_add(sum, Ordering::SeqCst);
            })
        );
    }

    for child in children {
        let _ = child.join();
    }
    print_duration(&timer.elapsed(), "scanned zeroes");
    println!("Zeroes {}", total_sum.load(Ordering::SeqCst));
}