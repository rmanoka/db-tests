extern crate db_tests;
extern crate tokio_core;
extern crate futures;
extern crate futures_state_stream;

use tokio_core::reactor::Core;
use futures::future::Future;
use futures_state_stream::StateStream;
use std::sync::atomic::{AtomicUsize, Ordering};
use self::db_tests::async;

fn main() {
    let sum = AtomicUsize::new(0);
    let mut l = Core::new().unwrap();
    let fut = async::establish_connection(&l.handle())
        .then(|c| {
            c
                .unwrap()
                .prepare("SELECT * FROM data")
        })
        .and_then(|(s,c)| {
            c.query(&s, &[])
                .for_each(|row| {
                    let datum : i32 = row.get(2);
                    if datum == 0 { sum.fetch_add(1, Ordering::SeqCst); }
                    // sum.fetch_add(datum as usize, Ordering::SeqCst);
                })
        });
    l.run(fut).unwrap();
    println!("Zeroes {}", sum.load(Ordering::SeqCst));
}