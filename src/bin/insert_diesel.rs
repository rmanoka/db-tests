extern crate diesel;
extern crate db_tests;

use self::db_tests::*;
use self::models::*;
use self::diesel::prelude::*;

use std::time::{Instant};

fn main() {
    use db_tests::schema::data::dsl::*;

    let connection = db::establish_connection();

    let timer = Instant::now();
    let random_data: Vec<DatumIn> = (0..8192).map(|_| {
        DatumIn::rand()
    }).collect();
    print_duration(&timer.elapsed(), "Generated data");

    let timer = Instant::now();
    for i in 0..256 {
        diesel::insert_into(data)
            .values(&random_data)
            .execute(&connection)
            .expect("Error inserting data");
        if i%8 == 0 {
            println!("Inserted for iteration {}", i);
        }
    }
    print_duration(&timer.elapsed(), "Inserted data");
}