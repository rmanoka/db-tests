extern crate postgres;
extern crate db_tests;
extern crate crc;

use std::env;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use std::time::{Instant};
use std::thread;

use db_tests::print_duration;
use postgres::{Connection, TlsMode};
use crc::crc64;

pub fn establish_connection() -> Connection {
    Connection::connect(
        env::var("DATABASE_URL")
            .expect("DATABASE_URL must be set"),
        TlsMode::None,
    ).unwrap()
}

fn do_sum(sql: &str) -> usize {
    let connection = establish_connection();
    let mut sum: usize = 0;
    for row in &connection.query(sql, &[]).unwrap() {
        let id: i32 = row.get(0);
        if id % 2 == 0 {
            let datum : i32 = row.get(2);
            sum += datum as usize;
        } else {
            let title: String = row.get(1);
            sum += title.len();
        }
    }
    return sum;
}

fn main() {
    const TOTAL : i64 = 2097152;
    const THREADS : i64 = 2;
    const LIMIT : i64 = TOTAL / THREADS;

    let total_sum = Arc::new(AtomicUsize::new(0));
    let timer = Instant::now();
    let mut children = vec![];

    for i in 0..THREADS {
        let total_sum = total_sum.clone();
        children.push(
            thread::spawn(move || {
                let sum = do_sum(
                    &format!(
                        "SELECT * FROM data OFFSET {} LIMIT {}",
                        i*LIMIT,
                        LIMIT
                    )
                );
                total_sum.fetch_add(sum, Ordering::SeqCst);
            })
        )
    }
    for child in children {
        let _ = child.join();
    }
    print_duration(&timer.elapsed(), "scanned zeroes");
    println!("Zeroes {}", total_sum.load(Ordering::SeqCst));
}