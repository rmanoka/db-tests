#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate rand;

pub mod schema;
pub mod models;
pub mod db;
pub mod async;

use std::time::{Duration};

pub fn print_duration(timer: &Duration, message: &str) {
    println!(
        "{}; time {}ms.",
        message,
        timer.as_secs() * 1000 + timer.subsec_millis() as u64
    );
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
