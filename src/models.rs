#[derive(Queryable)]
pub struct Datum {
    pub id: i32,
    pub title: String,
    pub datum: Option<i32>,
}

use schema::data;
#[derive(Insertable)]
#[table_name = "data"]
pub struct DatumIn {
    pub datum: Option<i32>,
    pub title: String,
}

impl DatumIn {
    pub fn rand() -> DatumIn {
        use rand::random;
        DatumIn {
            title: (0..1024).map(|_| (0x20u8 + (random::<u8>() % 96)) as char).collect(),
            datum: Some((random::<u32>() % 0x1000u32) as i32)
        }
    }
}